package com.company;

        import java.awt.Graphics;
        import java.awt.Image;
        import java.io.File;
        import java.io.IOException;

        import javax.imageio.ImageIO;
        import javax.swing.JFrame;
        import javax.swing.JPanel;

public class Game extends JPanel {



    public static final String NAME = "Title Game";
    public static final int WIDTH = 490;
    public static final int HEIGHT = 450;
    private Image image;

    public Game() {
        loadImage();
    }
    private void loadImage() {
        try {
            image = ImageIO.read(new File("labaratorGC.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void paint(Graphics g) {
        g.drawImage(image, 0, 0, null);
    }



    public static void main(String[] args){
        Game game = new Game();
        game.setSize(WIDTH, HEIGHT);
        JFrame frame = new JFrame(NAME);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(WIDTH, HEIGHT);
        frame.add(game);
        frame.setVisible(true);
    }
}