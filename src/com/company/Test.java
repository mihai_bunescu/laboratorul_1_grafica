package com.company;

import java.awt.*;
import java.awt.Graphics;
import java.awt.Polygon;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;
public class Test extends JPanel{

    public Test()
    {
        JFrame fereastra = new JFrame("Fereastra java");
        Component add = fereastra.add(this);
        fereastra.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        fereastra.setSize(1370,730);
        fereastra.setVisible(true);

        try
        {
            BufferedImage image = new BufferedImage(1370, 730, BufferedImage.TYPE_INT_RGB);
            Graphics graphics  = image.createGraphics();
            fereastra.paint(graphics);
            ImageIO.write(image,"bmp", new File("java1.bmp"));
        }
        catch(Exception exception)
        {
            //code
        }
    }


    protected void paintComponent(Graphics d)
    {
        //liniile care despat figurile
        super.paintComponent(d);
        this.setBackground(Color.WHITE);
        d.setColor(Color.BLUE);
        d.drawRect(10, 10, 1330, 670);
        d.drawLine(443, 10, 443, 680);
        d.drawLine(886, 10, 886, 680);
        d.drawLine(10,223,1340,223);
        d.drawLine(10, 446, 1340, 446);

        //Creez un paralepiped
        d.setColor(Color.BLACK);
        d.fillRect(140, 60, 110,120);
        d.drawLine(140, 60, 170, 40);
        d.drawLine(250, 60, 280, 40);
        d.drawLine(170, 40, 280, 40);
        d.drawLine(280, 40, 280, 160);
        d.drawLine(280, 160, 250, 180);

        // Creez un triunghi
        d.setColor(Color.BLACK);
        int[]x = new int[3];
        int[]y = new int[3];
        int n;

        x[0]=550; x[1]=600; x[2]=800;
        y[0]=60; y[1]=180; y[2]=160;
        n = 3;

        Polygon myTri = new Polygon(x, y, n);
        d.fillPolygon(myTri);


        //Creez un poligon
        Polygon q = new Polygon();

        for (int i = 0; i < 10; i++){
            q.addPoint((int) (2250/2+630/9 * Math.cos(i * 2 * Math.PI / 10)),
                    (int) (570/5 + 670/9  * Math.sin(i * 2 * Math.PI / 10)));
        }  d.drawPolygon(q);

        d.setColor(Color.GREEN);
        d.fillOval(150, 260,150, 150);

        d.setColor(Color.GREEN);
        d.fillArc(520, 180, 300, 240, 220, 100);

        d.setColor(Color.BLACK);
        d.drawRect(970, 260, 300, 150);


        //Cre
        d.setColor(Color.BLUE);
        d.fillOval(95, 480, 270, 170);  //drawOval

        //creez un sector de elipsa
        d.setColor(Color.BLUE);
        d.fillArc(480,510, 380, 130, 30, 120);

        //Creez o linie
        d.setColor(Color.BLACK);
        d.drawLine(5, 580, 1250, 550);

    }

    public static void main(String[] args)
    {
        Test test = new Test();
    }
}